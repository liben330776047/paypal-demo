package com.japhet.paypaldemo.config;

public enum PaypalPaymentMethod {
    credit_card, paypal
}
