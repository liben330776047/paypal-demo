package com.japhet.paypaldemo.config;

public enum PaypalPaymentIntent {
    sale, authorize, order
}
